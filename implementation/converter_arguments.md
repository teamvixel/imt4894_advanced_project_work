# IFC Convert.exe arguments

```sh
# Syntax
./IfcConvert.exe --site-local-placement --use-element-guids --no-normals <input_file> <output_file>

# Example
./IfcConvert.exe --site-local-placement --use-element-guids --no-normals 01_BIMcollab_Example_ARC.Ifc 01_BIMcollab_Example_ARC.Dae
```


