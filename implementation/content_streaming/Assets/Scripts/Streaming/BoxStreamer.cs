﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using IfcDatabase;
using UnityEngine;

public class BoxStreamer : MonoBehaviour
{
    private PMDB mDB;
    private List<GameObject> mRoots = new List<GameObject>();
    private List<IfcObject> mCurrentObjects = new List<IfcObject>();
    private Dictionary<string, GameObject> mGos = new Dictionary<string, GameObject>();

    private List<IfcObject> mObjectsToLoad = new List<IfcObject>();
    private List<IfcObject> mObjectsToUnload = new List<IfcObject>();

    private bool mRunning = true;

    [SerializeField]
    private BoxCollider mStreamBounds;
    private Vector3 mPrevQueryPosition;

    [SerializeField]
    private int mLoadCount = 5;
    [SerializeField]
    private int mUnloadCount = 5;

    // TODO: Needs further refinement. This way isn't really optimal.
    // TODO: Need to not create objects that were added to the queue but is no longer needed because they are outside
    // TODO: Need to not delete objects that are now within the correct bounds. (Also if they haven't been created the need to be switched to the other queue)
    // TODO: Should probably use something other than lists.

    // TODO: Load the db in its own thread.

    private void Start()
    {
        var foldername = $"{Application.dataPath}/Resources/ObjModels/BIMcollab/";
        mDB = new PMDB(foldername);
        mDB.PutObjectsIntoTree();

        StartCoroutine(LoadObjects());
        StartCoroutine(UnloadObjects());
    }

    private void OnDestroy()
    {
        mRunning = false;
    }

    private void Update()
    {
        if (mStreamBounds.gameObject.activeSelf)
        {
            if (Vector3.Distance(mPrevQueryPosition, mStreamBounds.bounds.center) >= mStreamBounds.bounds.size.x)
            {
                Debug.Log("Querrying");
                RunQuery();
            }
        }
    }

    private void RunQuery()
    {
        mPrevQueryPosition = mStreamBounds.bounds.center;

        var objects = mDB.GetObjectsWithin(mStreamBounds.bounds);
        
        var newObjects = objects.Except(mCurrentObjects).ToList();
        var objectsToDelete = mCurrentObjects.Except(objects).ToList();
        mObjectsToLoad.AddRange(newObjects);
        mObjectsToUnload.AddRange(objectsToDelete);
       
        Debug.Log($"Objects to delete count: {objectsToDelete.Count()}");
        Debug.Log($"First object to delete {objectsToDelete.FirstOrDefault()?.Guid}, {objectsToDelete.FirstOrDefault()?.MeshId}");

        mCurrentObjects.RemoveAll(x => objectsToDelete.Contains(x));
        mCurrentObjects.AddRange(newObjects);
    }

    private IEnumerator LoadObjects()
    {
        while (mRunning)
        {
            int count = 0;
            while (mObjectsToLoad.Count > 0 && count < 5)
            {
                var obj = mObjectsToLoad.First();
                mObjectsToLoad.RemoveAt(0);
                CreateObject(obj);
                count++;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator UnloadObjects()
    {
        while (mRunning)
        {
            int count = 0;
            while (mObjectsToUnload.Count > 0 && count++ < 5)
            {
                var obj = mObjectsToUnload.First();
                mObjectsToUnload.RemoveAt(0);
                
                if (mGos.ContainsKey(obj.Guid))
                {
                    var val = mGos[obj.Guid];
                    Debug.Log("Found someone to delete");
                    Destroy(val);
                    mGos.Remove(obj.Guid);
                }
                else
                {
                    Debug.Log($"Did not find object: {obj.Guid}");
                }
            }

            yield return new WaitForEndOfFrame();
        }
    }

    private void CreateObject(IfcObject ifcObject)
    {
        var parent = mRoots.Find(x => x.name == ifcObject.FolderName);

        if (parent == null)
        {
            parent = new GameObject(ifcObject.FolderName);
            parent.transform.eulerAngles = new Vector3(-90, 0, 0);
            parent.transform.localScale = new Vector3(-1, 1, 1);
            mRoots.Add(parent);
        }

        GameObject go = null;
        try
        {
            go = Instantiate(Resources.Load($"ObjModels/BIMcollab/{ifcObject.FolderName}/{ifcObject.MeshId}", typeof(GameObject)) as GameObject);
            go.transform.parent = parent.transform;
            go.name = ifcObject.Guid;
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = ifcObject.Position;
            go.transform.localRotation = ifcObject.Rotation;

            var child = go.transform.GetChild(0);
            child.transform.parent = go.transform;
            child.transform.localPosition = Vector3.zero;
            child.transform.localRotation = Quaternion.identity;
            child.transform.localScale = new Vector3(-1, 1, 1);

            var renderer = child.GetComponent<MeshRenderer>();
            renderer.receiveShadows = false;
            renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

            mGos.Add(go.name, go);
        }
        catch (Exception e)
        {
            Debug.LogWarning($"Failed to load object: {ifcObject.Guid} with mesh {ifcObject.FolderName}/{ifcObject.MeshId}");
            Destroy(go);
        }
    }

    private void OnDrawGizmosSelected()
    {
        mDB?.DrawTree();
    }

    [ContextMenu("Unload Assets")]
    private void UnloadAssets()
    {
        Resources.UnloadUnusedAssets();
    }
}
