﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using IfcDatabase;
using System.IO;

public class PMStreamer : MonoBehaviour
{
    // Number of objects to load per press of 2
    public int LoadCount = 0;

    PMDB mDB;
    List<GameObject> mRoots = new List<GameObject>();

    List<IfcObject> mCurrentObjects;

    int mVertexCount = 0;
    int mGameObjectsCount = 0;
    int mTotalGameObjects = 0;

    void Update()
    {
        UnityEngine.Profiling.Profiler.BeginSample("Load Objects");

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            var sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
            var foldername = $"{Application.dataPath}/Resources/ObjModels/BIMcollab/";
            mDB = new PMDB(foldername);
            var info = new DirectoryInfo(foldername);
            
            Debug.Log("Loading finished");

            Debug.Log($"{mDB.GetObjects(Vector3.zero).Count}");

        }
        UnityEngine.Profiling.Profiler.EndSample();

        // Ask to get a new list based on the position
        UnityEngine.Profiling.Profiler.BeginSample("Instantiate objects");
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            StartCoroutine(LoadObjects());
            //LoadObjects();
        }
        UnityEngine.Profiling.Profiler.EndSample();


        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            mDB.PutObjectsIntoTree();
            //StartCoroutine(mDB.PutObjectsIntoTree());
        }

        mDB?.DrawTree();

    }

    IEnumerator LoadObjects()
    {
        mCurrentObjects = mDB.GetObjects(Vector3.zero);

        int i = 0;
        foreach (var ifcObject in mCurrentObjects)
        {
            var parent = mRoots.Find(x =>
            {
                return x.name == ifcObject.FolderName;
            });

            if (parent == null)
            {
                parent = new GameObject(ifcObject.FolderName);
                parent.transform.eulerAngles = new Vector3(-90, 0, 0);
                parent.transform.localScale = new Vector3(-1, 1, 1);
                mRoots.Add(parent);
            }

            var go = Instantiate(Resources.Load($"ObjModels/BIMcollab/{ifcObject.FolderName}/{ifcObject.MeshId}", typeof(GameObject)) as GameObject);
            go.transform.parent = parent.transform;
            go.name = ifcObject.Guid;
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = ifcObject.Position;
            go.transform.localRotation = ifcObject.Rotation;

            var child = go.transform.GetChild(0);
            child.transform.parent = go.transform;
            child.transform.localPosition = Vector3.zero;
            child.transform.localRotation = Quaternion.identity;
            child.transform.localScale = new Vector3(-1, 1, 1);

            var renderer = child.GetComponent<MeshRenderer>();
            renderer.receiveShadows = false;
            renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

            if (i++ % 15 == 0)
                yield return new WaitForSecondsRealtime(0.01f);

        }


        Debug.Log($"Go count: {mTotalGameObjects}, vertex count: {mVertexCount}");


        Debug.Log("Finished");
    }

    [ContextMenu("Unload Assets")]
    void UnloadAssets()
    {
        Resources.UnloadUnusedAssets();
    }
}
