﻿using UnityEngine;

namespace IfcDatabase
{
    // Should make this a struct at some point.
    public unsafe class IfcObject
    {
        public string FolderName;
        public string Guid;
        public string ParentId;
        public string MeshId;
        public string Type; // Should be enum!
        public Vector3 QueryUnityPos; // Hack for working with positions similar to those we scale based upon.
        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Scale;

        public IfcObject()
        {
            FolderName = null;
            Guid = null;
            ParentId = null;
            MeshId = null;
            Position = Vector3.zero;
            Rotation = Quaternion.identity;
            Scale = Vector3.one;
        }

        public IfcObject(string ifcGuid)
        {
            FolderName = null;
            Guid = ifcGuid;
            ParentId = null;
            MeshId = null;
            Position = Vector3.zero;
            Rotation = Quaternion.identity;
            Scale = Vector3.one;
        }
    }
}
