﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

namespace IfcDatabase
{
    // TODO: Add a way to attach more files to the DB while it is running
    public class PMDB
    {
        List<IfcObject> mIfcObjects = new List<IfcObject>();
        
        Octree mOctree;

        // Instead of taking a single file, take a folder, and go through all the .pmdb files in that folder?
        // Or take an array of filenames?

        public PMDB(string foldername)
        {
            Debug.Log("Something");

            int count = 0;
            var info = new DirectoryInfo(foldername);
            var foldernames = new List<string>();
            foreach (var filename in info.GetFiles("*.pmdb", SearchOption.AllDirectories))
            //foreach (var filename in info.GetFiles("ARK ABC.pmdb", SearchOption.AllDirectories))
            {
                using (var stream = new StreamReader(filename.FullName))
                {
                    string line;
                    while ((line = stream.ReadLine()) != null)
                    {
                        var cols = line.Split(' ');
                        var obj = new IfcObject();
                        obj.Guid = cols[0];
                        obj.Type = cols[1];
                        obj.ParentId = (cols[2] != "none") ? cols[2] : null;
                        obj.MeshId = cols[3];
                        obj.FolderName = filename.Directory.Name;

                        // Read 4 more lines, make assumption that we don't get half objects.
                        Matrix4x4 mat = new Matrix4x4();
                        for (int i = 0; i < 4; i++)
                        {
                            var row = stream.ReadLine().Split(' ');
                            var vec = new Vector4(float.Parse(row[0]), float.Parse(row[1]), float.Parse(row[2]), float.Parse(row[3]));
                            mat.SetRow(i, vec);
                        }

                        obj.Position = mat.MultiplyPoint3x4(Vector3.zero);
                        var euler = mat.rotation.eulerAngles;
                        obj.Rotation = Quaternion.Euler(euler.x, euler.y, euler.z);
                        obj.QueryUnityPos = ToUnityCoords(obj.Position);

                        mIfcObjects.Add(obj);

                        if (count++ >= 50)
                        {
                            //break;
                        }
                    }
                }
            }

            mOctree = new Octree(new Bounds(ToUnityCoords(new Vector3(0, 0, 0)), new Vector3(125f, 125f, 125f)));

            Debug.Log("Finished Loading in files, Finding bounds for tree");

        }

        public List<IfcObject> GetObjects(Vector3 pos)
        {
            return mIfcObjects;
            //var list = new List<IfcObject>();

            //foreach (var store in mIfcDBS)
            //{
            //    var itr = store.Instances.OfType<IIfcBuildingElement>().GetEnumerator();
            //    while (itr.MoveNext())
            //    {
            //        var obj = new IfcObject(itr.Current.GlobalId.Value.ToString());

            //        var objPos = GetPosition(((IfcLocalPlacement)itr.Current.ObjectPlacement));
            //        objPos = ToUnityCoords(objPos);

            //        if (Vector3.Distance(pos, objPos) < 20)
            //            list.Add(obj);

            //    }
            //}

            //return list;
        }

        public List<IfcObject> GetObjectsWithin(Bounds bounds)
        {
            return mOctree.GetAllObjectsWithin(bounds);
        }

        private Vector4 ToUnityCoords(Vector4 ifcPos)
        {
            // orig
            //return new Vector3(-ifcPos.x * 0.001f, ifcPos.z * 0.001f, -ifcPos.y * 0.001f);

            //
            return new Vector4(-ifcPos.x, ifcPos.z, -ifcPos.y, ifcPos.w);

            //return ifcPos;
        }

        public IEnumerator PutObjectsIntoTreeAsync()
        {
            for (int i = 0; i < mIfcObjects.Count; i++)
            {
                if (!mOctree.Insert(mIfcObjects[i]))
                {
                    Debug.LogWarning($"Could not insert object {mIfcObjects[i]}");
                }
                if (i % 5 == 0)
                {
                    yield return new WaitForSecondsRealtime(0.01f);
                }
            }

            Debug.Log("Finished Creation of Octree");

        }

        public void PutObjectsIntoTree()
        {
            for (int i = 0; i < mIfcObjects.Count; i++)
            {
                if (!mOctree.Insert(mIfcObjects[i]))
                {
                    Debug.LogWarning($"Could not insert Object: {mIfcObjects[i].Guid}, Pos: {mIfcObjects[i].QueryUnityPos}");
                }
            }

            Debug.Log("Finished Creation of Octree");
        }

        public void DrawTree()
        {
            mOctree?.DrawBounds();
        }
    }
}

