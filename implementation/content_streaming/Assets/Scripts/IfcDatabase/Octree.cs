﻿using System.Collections.Generic;
using IfcDatabase;
using UnityEngine;

public class Octree
{
    private Bounds mBounds;

    private const int MaxChildCount = 8;
    private const int MaxObjectCount = 8;

    private List<IfcObject> mObjects = new List<IfcObject>();
    private List<Octree> mChildren = new List<Octree>();

    public Octree(Bounds bounds)
    {
        mBounds = bounds;
    }

    public void DrawBounds()
    {
        // This needs to be updated to be drawn in 3 dimensions, currently it is only drawn in 2.
        // Draw forward facing

        Utility.BoundsDrawer.Draw(mBounds, Color.red);

        foreach (var child in mChildren)
        {
            child.DrawBounds();
        }
    }

    private void SubDivide()
    {
        {
            var botLeftCenter = mBounds.center;
            botLeftCenter.x = mBounds.center.x - mBounds.extents.x * 0.5f;
            botLeftCenter.y = mBounds.center.y - mBounds.extents.y * 0.5f;
            botLeftCenter.z = mBounds.center.z - mBounds.extents.z * 0.5f;
            mChildren.Add(new Octree(new Bounds(botLeftCenter, mBounds.size / 2)));


            var botRightCenter = botLeftCenter;
            botRightCenter.x = mBounds.center.x + mBounds.extents.x * 0.5f;
            botRightCenter.y = mBounds.center.y - mBounds.extents.y * 0.5f;
            botRightCenter.z = mBounds.center.z - mBounds.extents.z * 0.5f;
            mChildren.Add(new Octree(new Bounds(botRightCenter, mBounds.size / 2)));

            var topLeftCenter = botLeftCenter;
            topLeftCenter.x = mBounds.center.x - mBounds.extents.x * 0.5f;
            topLeftCenter.y = mBounds.center.y + mBounds.extents.y * 0.5f;
            topLeftCenter.z = mBounds.center.z - mBounds.extents.z * 0.5f;
            mChildren.Add(new Octree(new Bounds(topLeftCenter, mBounds.size / 2)));

            var topRightCenter = botRightCenter;
            topRightCenter.x = mBounds.center.x + mBounds.extents.x * 0.5f;
            topRightCenter.y = mBounds.center.y + mBounds.extents.y * 0.5f;
            topRightCenter.z = mBounds.center.z - mBounds.extents.z * 0.5f;
            mChildren.Add(new Octree(new Bounds(topRightCenter, mBounds.size / 2)));
        }
        {
            var botLeftCenter = mBounds.center;
            botLeftCenter.x = mBounds.center.x - mBounds.extents.x * 0.5f;
            botLeftCenter.y = mBounds.center.y - mBounds.extents.y * 0.5f;
            botLeftCenter.z = mBounds.center.z + mBounds.extents.z * 0.5f;
            mChildren.Add(new Octree(new Bounds(botLeftCenter, mBounds.size / 2)));


            var botRightCenter = botLeftCenter;
            botRightCenter.x = mBounds.center.x + mBounds.extents.x * 0.5f;
            botRightCenter.y = mBounds.center.y - mBounds.extents.y * 0.5f;
            botRightCenter.z = mBounds.center.z + mBounds.extents.z * 0.5f;
            mChildren.Add(new Octree(new Bounds(botRightCenter, mBounds.size / 2)));

            var topLeftCenter = botLeftCenter;
            topLeftCenter.x = mBounds.center.x - mBounds.extents.x * 0.5f;
            topLeftCenter.y = mBounds.center.y + mBounds.extents.y * 0.5f;
            topLeftCenter.z = mBounds.center.z + mBounds.extents.z * 0.5f;
            mChildren.Add(new Octree(new Bounds(topLeftCenter, mBounds.size / 2)));

            var topRightCenter = botRightCenter;
            topRightCenter.x = mBounds.center.x + mBounds.extents.x * 0.5f;
            topRightCenter.y = mBounds.center.y + mBounds.extents.y * 0.5f;
            topRightCenter.z = mBounds.center.z + mBounds.extents.z * 0.5f;
            mChildren.Add(new Octree(new Bounds(topRightCenter, mBounds.size / 2)));
        }
    }

    public bool Insert(IfcObject ifcObject)
    {
        if (!mBounds.Contains(ifcObject.QueryUnityPos))
            return false;

        if (mObjects.Count < MaxObjectCount)
        {
            mObjects.Add(ifcObject);
            return true;
        }

        if (mChildren.Count < MaxChildCount)
            SubDivide();

        foreach (var child in mChildren)
        {
            if (child.Insert(ifcObject))
                return true;
        }

        return false;
    }

    public List<IfcObject> GetAllObjectsWithin(Bounds bounds)
    {
        var objects = new List<IfcObject>();
        if (!mBounds.Intersects(bounds))
            return objects;

        objects.AddRange(mObjects.FindAll(x => bounds.Contains(x.QueryUnityPos)));

        foreach (var child in mChildren)
        {
            objects.AddRange(child.GetAllObjectsWithin(bounds));
        }

        return objects;
    }
}
