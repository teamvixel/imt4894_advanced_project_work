﻿using UnityEngine;
using System.Collections;

namespace Utility
{
    public class BoundsDrawer : MonoBehaviour
    {
        public static void Draw(Bounds bounds, Color color)
        {
            var botLeft = bounds.center;
            botLeft.x = bounds.center.x - bounds.extents.x;
            botLeft.y = bounds.center.y - bounds.extents.y;
            botLeft.z = bounds.center.z - bounds.extents.z;

            var botRight = bounds.center;
            botRight.x = bounds.center.x + bounds.extents.x;
            botRight.y = bounds.center.y - bounds.extents.y;
            botRight.z = bounds.center.z - bounds.extents.z;

            var topLeft = bounds.center;
            topLeft.x = bounds.center.x - bounds.extents.x;
            topLeft.y = bounds.center.y + bounds.extents.y;
            topLeft.z = bounds.center.z - bounds.extents.z;

            var topRight = bounds.center;
            topRight.x = bounds.center.x + bounds.extents.x;
            topRight.y = bounds.center.y + bounds.extents.y;
            topRight.z = bounds.center.z - bounds.extents.z;

            Debug.DrawLine(botLeft, botRight, Color.red);
            Debug.DrawLine(botLeft, topLeft, Color.red);
            Debug.DrawLine(botRight, topRight, Color.red);
            Debug.DrawLine(topLeft, topRight, Color.red);

            var newBotLeft = botLeft;
            var newBotRight = botRight;
            var newTopLeft = topLeft;
            var newTopRight = topRight;

            newBotLeft.z = bounds.center.z + bounds.extents.z;
            newBotRight.z = bounds.center.z + bounds.extents.z;
            newTopLeft.z = bounds.center.z + bounds.extents.z;
            newTopRight.z = bounds.center.z + bounds.extents.z;

            Debug.DrawLine(newBotLeft, newBotRight, Color.red);
            Debug.DrawLine(newBotLeft, newTopLeft, Color.red);
            Debug.DrawLine(newBotRight, newTopRight, Color.red);
            Debug.DrawLine(newTopLeft, newTopRight, Color.red);

            Debug.DrawLine(botLeft, newBotLeft, Color.red);
            Debug.DrawLine(botRight, newBotRight, Color.red);
            Debug.DrawLine(topLeft, newTopLeft, Color.red);
            Debug.DrawLine(topRight, newTopRight, Color.red);
        }
    }
}
