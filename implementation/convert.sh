#!/bin/bash

# TODO: Move over to just putting the DAE files in Unity DaeModels, rather than the temporary loation(?)
#       To avoid wasting so much memory.

OIFS="$IFS"
IFS=$'\n'

export master_path=/d/School/HiG/master/pm_master_implementation/implementation;
export ifc_path=${master_path}/models/ifc_models/${1};
export dae_path=${master_path}/models/dae_models/${1};
export obj_path=${master_path}/models/obj_models/${1};
export unity_dae_path=${master_path}/content_streaming/Assets/DaeModels/${1};
export unity_ifc_path=${master_path}/content_streaming/Assets/StreamingAssets/IfcModels/${1};
export unity_obj_path=${master_path}/content_streaming/Assets/Resources/ObjModels/${1}

pushd ${master_path} > /dev/null
# Functioning, but single threaded!
#for file in $(ls ${ifc_path})
#do
#
#    # Isolate filename
#    filename=$(basename -- "$file")
#    extension="${filename##*.}"
#    foldername="${filename%.*}"
#    mkdir -p "${obj_path}/${foldername}"
#
#    # Convert
#    dae_filename=${file::-4}.dae;
#    #printf "y\n" | ./IfcConvert.exe --site-local-placement --use-element-guids --no-normals ${ifc_path}/${file} ${obj_path}/${foldername}/${dae_filename}
#
#    #rm -f "${obj_path}/${foldername}/${dae_filename}"
#
#    # Move to Unity folder
#    mkdir -p "${unity_obj_path}/${foldername}"
#    cp -r "${obj_path}/${foldername}" "${unity_obj_path}/${foldername}"
#
#    #cp ${dae_path}/${dae_filename} ${unity_dae_path}/${dae_filename}
#    #cp ${ifc_path}/${file} ${unity_ifc_path}/${file}
#done

function convert_func()
{
    file="${1}"
    echo "${1}"

    # Isolate filename
    filename=$(basename -- "$file")
    extension="${filename##*.}"
    foldername="${filename%.*}"
    #mkdir -p "${obj_path}/${foldername}"

    #echo "${foldername}"

    echo "${ifc_path}/${file} ${obj_path}/${foldername}/${dae_filename}"

    # Convert
    dae_filename=${file::-4}.dae;
    # Don't use --site-local, but rather offset when loading into Unity
    printf "y\n" | ./IfcConvert.exe --use-element-guids --no-normals "${ifc_path}/${file}" "${obj_path}/${foldername}/${dae_filename}"

    rm -f "${obj_path}/${foldername}/${dae_filename}"

    # Move to Unity folder
    #mkdir -p "${unity_obj_path}/${foldername}"
    cp -r "${obj_path}/${foldername}" "${unity_obj_path}/${foldername}"

    #cp ${dae_path}/${dae_filename} ${unity_dae_path}/${dae_filename}
    #cp ${ifc_path}/${file} ${unity_ifc_path}/${file}
}

export -f convert_func

mkdir -p "${unity_obj_path}/"

for file in $(ls ${ifc_path})
do

    # Isolate filename
    filename=$(basename -- "$file")
    extension="${filename##*.}"
    foldername="${filename%.*}"
    mkdir -p "${obj_path}/${foldername}"

    echo "${file}"
# Run this function in 7 processes.
done | xargs --max-procs 7 -I{} bash -c 'convert_func "{}"'


IFS="$OIFS"
popd > /dev/null

# Thoughts for extension
# Do jobs in background, make note of which folder is dedicated ot which job
# When a job is finished, find out which folder it is and copy it. This way Unity can start importing while the jobs are all still running.
# https://thoughtsimproved.wordpress.com/2015/05/18/parellel-processing-in-bash/
