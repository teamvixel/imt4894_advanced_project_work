# IMT4894 - Advanced Project Work: Streaming BIM Models in VR, Per-Morten Straume
This repository contains the code for the project "Streaming BIM models in VR", by Per-Morten Straume.
The proper history for this project is lacking, as I kept IFC models I could not publish in the original private repository.
The project has made use of git lfs, so make sure you have it installed to pull all the files tracked through it.

# Relevant Files & Folders
The implementation folder contains the folders relevant for the different aspects of the project.

## content_streaming
The content_streaming folder contains the implementation of the streaming logic.
To run the project you need the Unity game engine installed. 
I developed the project using Unity 2018.2, but earlier versions might work.
To set up the project you need to unzip the ObjModels.zip file located in "./implementation/content_streaming/Assets/Resources/", just unzip straight into the folder where the ObjModels.zip is located.
Setting up the project will take a while, as all the .obj files needs to be imported (only once per project setup).
The most important scripts are the BoxStreamer, PMDB, IfcObject, and Octree files as they implement the streaming functionality.

### Camera movement.
You can move camera using the WASDQE keys while holding down the right mousebutton. Holding down the left mousebutton and using the same keys will move the cube used for streaming.

## IfcOpenShell
The IfcOpenShell folder contains the IfcOpenShell codebase, with modifications to support outputing .obj files. 
Mainly the modifications have been done in the src/ifcconvert/IfcConvert.cpp file, with some minor modifications to the WavefrontObjSerializer.cpp file. The functions "create_obj_files" and "output_to_db" and a call to this function has been added in the main function of IfcConvert.cpp.
Instructions on how to build the executable can be found at: [https://github.com/IfcOpenShell/IfcOpenShell](https://github.com/IfcOpenShell/IfcOpenShell)  
However, a build executable (IfcConvert.exe) has already been supplied.

## Convert.sh
The convert.sh script can be used to convert .ifc files to .obj files. It requires that you modify the variable "master_path" to point to the directory containing the convert.sh script.
The script can be used by running the following command in a git bash terminal standing in the implementation folder: 
```
./convert.sh <FoldernameOfBimModel> 
# Example
./convert.sh BimCollab

```

